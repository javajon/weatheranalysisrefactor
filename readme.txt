This directory contains examples and lab created at the Measuring Quality of Design workshop at ArchConf 2015.

Venkat Subramaniam
http://www.agiledeveloper.com
venkats@agiledeveloper.com
Twitter: @venkat_s

Join the No Fluff Just Stuff Software Symposium Tour for a great experience!
https://nofluffjuststuff.com/

Local modifications were added for Gradle wrapper
