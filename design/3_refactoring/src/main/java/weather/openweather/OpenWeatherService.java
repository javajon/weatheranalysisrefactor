package weather.openweather;
import weather.service.*;

import weather.*;
import java.util.*;
import java.net.*;
import org.json.*;

public class OpenWeatherService implements WeatherService {
  public WeatherData getWeatherData(String cityName) {
    try {
      return parseResponse(getRawData(cityName));
    } catch(Exception ex) {
      return new WeatherData(cityName, ex);
    }
  }
  
  private String getRawData(String cityName) throws Exception {
    URL url = new URL("http://api.openweathermap.org/data/2.5/weather?q=" + cityName + ",us&units=imperial&APPID=5d21f97480dbcad499959854a97617d4");

    Scanner scanner = new Scanner(url.openStream());
    return scanner.nextLine();
  }
  
  private WeatherData parseResponse(String response) {
    JSONObject weatherDetails = new JSONObject(response);
    String condition = weatherDetails.getJSONArray("weather").getJSONObject(0).getString("description");
    
    return new WeatherData(weatherDetails.getString("name"), weatherDetails.getJSONObject("main").getDouble("temp"), condition);      
  }
}
