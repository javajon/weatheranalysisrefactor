package weather.ui;
import weather.*;
import weather.service.*;

import java.util.*;
import java.io.*;
import weather.*;
import weather.openweather.*;

public class WeatherReport {
  public static void main(String[] args) {
    List<String> cityNames = new ArrayList<>();
    try {
      cityNames = readCities();
    } catch(Exception ex) {
      System.out.println("Error reading file...");
      System.out.println(ex.getMessage());
      System.exit(1);      
    }
    
    Collections.sort(cityNames);
    
    WeatherGatherer weatherGatherer = new WeatherGatherer(
      new OpenWeatherService());
    WeatherAggregate weatherAggregate = weatherGatherer.getDataForCities(cityNames);
    
    printWeatherData(weatherAggregate.weatherData);
    
    printCities("Warmest Cities", weatherAggregate.warmestCities);
    printCities("Coldest Cities", weatherAggregate.coldestCities);
    
    printError(weatherAggregate.errors);
  }
  
  public static void printWeatherData(List<WeatherData> weatherData) {
    System.out.println("Weather details");
    System.out.printf("%-20s %-12s %s\n", "City", "Temperature", "Condition");

    for(WeatherData weatherDataForCity : weatherData) {
      System.out.printf("%-20s %-12s %s\n", weatherDataForCity.getCity(), weatherDataForCity.getTemperature(), weatherDataForCity.getCondition());
    }    
  }
  
  public static void printCities(String message, List<String> cities) {
    System.out.println("");
    System.out.println(message);
    for(int i = 0; i < cities.size(); i++) {
      System.out.print(cities.get(i));
      if(i != cities.size() - 1)
        System.out.print(", ");
    }        
  }
  
  public static void printError(List<WeatherData> errors) {
    System.out.println("");
    System.out.println("Cities that Failed");
    for(WeatherData weatherDataForCity : errors) {
      System.out.printf("%-20s %s\n", weatherDataForCity.getCity(), weatherDataForCity.getError().getMessage());
    }    
  }
  
  public static List<String> readCities() throws Exception {
    List<String> cityNames = new ArrayList<String>();
    
    BufferedReader reader = new BufferedReader(new FileReader("cities.txt"));
    String line = null;
    while((line = reader.readLine()) != null) {
      if(!line.startsWith("#"))
        cityNames.add(line);
    }
    return cityNames;
  }
}
