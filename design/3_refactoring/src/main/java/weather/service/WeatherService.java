package weather.service;

public interface WeatherService {
  public WeatherData getWeatherData(String cityName);
}
