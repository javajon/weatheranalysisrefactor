package weather.service;

public class WeatherData {
  private final String city;
  private final double temperature;
  private final String condition;
  private final Exception error;
  
  public WeatherData(String theCity, double theTemperature, String theCondition) {
    city = theCity;
    temperature = theTemperature;
    condition = theCondition;
    error = null;
  }
  
  public WeatherData(String theCity, Exception theException) {
    city = theCity;
    error = theException;
    temperature = 0;
    condition = "";
  }
  
  public boolean isError() { return error != null; }
  public String getCity() { return city; }
  public double getTemperature() { return temperature; }
  public String getCondition() { return condition; }
  public Exception getError() { return error; }
}

