package weather;
import weather.service.*;

import java.util.*;

public class WeatherAggregate {
  public final List<WeatherData> weatherData;
  public final List<String> warmestCities;
  public final List<String> coldestCities;
  public final List<WeatherData> errors;
  
  public WeatherAggregate(List<WeatherData> data,
      List<String> warmest, List<String> coldest,
      List<WeatherData> theErrors) {
    weatherData = data;
    warmestCities = warmest;
    coldestCities = coldest;
    errors = theErrors;
  }
}
