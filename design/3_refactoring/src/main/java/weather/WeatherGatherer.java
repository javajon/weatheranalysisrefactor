package weather;
import weather.service.*;

import java.util.*;

public class WeatherGatherer {
  private final WeatherService weatherService;
  
  public WeatherGatherer(WeatherService aWeatherService) {
    weatherService = aWeatherService;
  }
  
  private List<String> getWarmestCities(List<WeatherData> weatherData) {
    List<String> cities = new ArrayList<>();
    double maxTemperature = -1000;
    
    for(WeatherData weatherForACity : weatherData) {
      if(weatherForACity.getTemperature() > maxTemperature) {
        maxTemperature = weatherForACity.getTemperature();
        cities.clear();
      }
      
      if(weatherForACity.getTemperature() == maxTemperature)
        cities.add(weatherForACity.getCity());
    }
    return cities;
  }

  private List<String> getColdestCities(List<WeatherData> weatherData) {
    List<String> cities = new ArrayList<>();
    double minTemperature = 1000;
    
    for(WeatherData weatherForACity : weatherData) {
      if(weatherForACity.getTemperature() < minTemperature) {
        minTemperature = weatherForACity.getTemperature();
        cities.clear();
      }
      
      if(weatherForACity.getTemperature() == minTemperature)
        cities.add(weatherForACity.getCity());
    }
    return cities;
  }
  
  
  public WeatherAggregate getDataForCities(List<String> cityNames) {
    List<WeatherData> validData = new ArrayList<WeatherData>();
    List<WeatherData> errors = new ArrayList<WeatherData>();
    
    for(String cityName: cityNames) {
      WeatherData data = weatherService.getWeatherData(cityName);
      
      if(data.isError())
        errors.add(data);
      else
        validData.add(data);
    }
    
    return new WeatherAggregate(
      validData,
      getWarmestCities(validData),
      getColdestCities(validData),
      errors);
  }
}
